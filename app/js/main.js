// Require JS  Config File
require({
		baseUrl: 'js/',
		paths: {
			 'angular': '../lib/angular/index'
			, 'angular-mobile': '../lib/angular-mobile/index'
			, 'angular-resource': '../lib/angular-resource/index'
			, 'jquery': '../lib/jquery/jquery'
		},
		shim: {
			'app': {
				'deps': [
					'angular'
					, 'angular-resource'
					, 'angular-mobile'
				]
			},
			'angular-resource': { 'deps': ['angular','jquery'], 'exports':'angular' },
			'angular-mobile': { 'deps': ['angular'] },
			'routes': { 'deps': [
				'app'
			]},
			'controllers/ApplicationController': {
				'deps': [
					'app'
				]}
		}
	},
	[
		'require'
		, 'routes'
		, 'controllers/ApplicationController'

	],
	function (require) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);
