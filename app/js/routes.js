/**
 * # User-facing URL Router
 *
 * Specified what URLs are available to a user, and what controllers should be
 * assigned to each.
 */
angular.module('app').config( function( $routeProvider , $locationProvider, $httpProvider ) {


		delete $httpProvider.defaults.headers.common['X-Requested-With'];

		$routeProvider
		.when
		(	'/'
			,{ templateUrl: 'views/dashboard.html'
			, controller: 'ApplicationController'
			}
		)
		.when
		(	'/dashboard'
			,{ templateUrl: 'views/dashboard.html'
			 , controller: 'ApplicationController'
			 }
		)
		.when
		(	'/reports'
			,{ templateUrl: 'views/reports.html'
			 , controller: 'ApplicationController'
			 }
		)
		.when
		(	'/scheduling'
			,{ templateUrl: 'views/scheduling.html'
			 , controller: 'ApplicationController'
			 }
		)
		.when
		(	'/scheduling-step-2'
		  ,{ templateUrl: 'views/scheduling-step-2.html'
		  , controller: 'ApplicationController'
		   }
		)
		.when
		(	'/scheduling-step-3'
			,{ templateUrl: 'views/scheduling-step-3.html'
				 , controller: 'ApplicationController'
			 }
		)
		.when
		(	'/settings'
			,{ templateUrl: 'views/settings.html'
			 , controller: 'ApplicationController'
			 }
		)



		/**
		 * ## HTML5 pushState support
		 *
		 * This enables urls to be routed with HTML5 pushState so they appear in a
		 * '/someurl' format without a page refresh
		 *
		 * The server must support routing all urls to index.html as a catch-all for
		 * this to function properly,
		 *
		 * The alternative is to disable this which reverts to '#!/someurl'
		 * anchor-style urls.
		 */
		//$locationProvider.html5Mode(true)
	}
)


