/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController'
	,
	function
		(
			$rootScope
			, $scope
			, $log
			, $timeout
			, $location
			, $routeParams
			, $window
			) {

		$rootScope.siteName = 'Reporting'
		$scope.currentView = 'dashboard'
		$scope.showDropdown = false

		$scope.routeTo = function (view){
			$scope.currentView = view;
			$location.path('/' + view);
		}

		angular.element(document.getElementById('searchBox') ).bind('click', function (){
			angular.element(document.getElementById('seachResults') ).css({'display':'inline'})
		})

		angular.element(document.getElementById('searchBox') ).bind('focusout', function (){
			angular.element(document.getElementById('seachResults') ).css({'display':'none'})
		})

		$scope.$on('$routeChangeSuccess', function(){
			if($location.$$path.substr(1) != "")
			$scope.currentView = $location.$$path.substr(1)
		})

		$scope.$on('$routeChangeStart', function(){
		})

	}
)
